<?php
/**
 * Telegram plugin
 *
 * @package     Telegram
 * @author      Marko Banusic
 * @copyright   2017 Telegram Media Grupa
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Telegram plugin
 * Plugin URI:  https://telegram.hr
 * Description: Basic functionality for Telegram portal
 * Version:     2.0.0
 * Author:      Marko Banusic
 * Author URI:  https://mbanusic.com
 * Text Domain: telegram
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

function telegram_plugin() {

		require_once dirname(__FILE__) . '/vendor/autoload.php';

		return Telegram\PortalPlugin\Init::instance();
}
telegram_plugin();
