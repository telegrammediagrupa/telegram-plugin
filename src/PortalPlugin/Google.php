<?php
namespace Telegram\PortalPlugin;


class Google {
	private $client;

	public function __construct() {
		$this->load_client();
	}

	private function load_client() {
		$key_file_location = '/home/marko/telegram/Telegram-618e00d5592f.json';

		$this->client = new \Google_Client();
		$this->client->setApplicationName("Telegram");

		$this->client->setAuthConfig($key_file_location);
		$this->client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	}

	private function get_post_by_slug($slug) {
		global $wpdb;
		$id = false;
		if ($slug) {
			$id = $wpdb->get_var( "SELECT ID FROM {$wpdb->posts} WHERE  post_name LIKE '{$slug}' LIMIT 1" );
		}
		return $id;
	}

	public function get_most_read() {
		$service = new \Google_Service_Analytics($this->client);
		$opts = array(
			'dimensions' => 'ga:pagePath',
			'sort' => '-ga:pageviews',
			'max-results' => 50
		);
		//telegram
			$data = $service->data_ga->get( 'ga:194731141', date( 'Y-m-d', strtotime( '-2 days' ) ), date( 'Y-m-d' ), 'ga:pageviews', $opts );
			if ( count($data->getRows()) > 0 ){

				foreach ($data->getRows() as $row) {

					$link = $row[0];
					if (substr($link, -1) == '/') {
						$link = substr($link,0,-1);
					}
					$slug = explode('/', $link);
					$slug = array_pop($slug);
					$id = $this->get_post_by_slug($slug);
					if ($id) {
						$post_time = get_post_time('U', false, $id);
						if ( $post_time > ( current_time('U') - 7 * 24 * 3600 ) ) {
							if ($id == 784373) {
								continue;
							}
							$out[] = intval($id);
						}
					}
				}
				if (!empty($out)) {
					update_option('telegram_most_read', $out);
				}
				update_option('telegram_most_read_time', date('d-m-Y H:i'));
			}
	}

	public function get_realtime() {
		$service = new \Google_Service_Analytics($this->client);
		$opts = array(
			'dimensions' => 'rt:pagePath',
			'sort' => '-rt:pageViews',
			'max-results' => 50
		);
			$data = $service->data_realtime->get( 'ga:194731141', 'rt:pageViews', $opts );
			if ( count($data->getRows()) > 0 ){

				foreach ($data->getRows() as $row) {

					$link = $row[0];
					if (substr($link, -1) == '/') {
						$link = substr($link,0,-1);
					}
					$slug = explode('/', $link);
					$slug = array_pop($slug);
					$id = $this->get_post_by_slug($slug);
					if ($id) {

						if ($id == 784373) {
							continue;
						}
						$out[] = intval($id);
					}
				}
				if (!empty($out)) {
					update_option('telegram_realtime', $out);
				}
			}
		switch_to_blog(2);
		$data = $service->data_realtime->get( 'ga:132592657', 'rt:pageViews', $opts );
		if ( count($data->getRows()) > 0 ){

			foreach ($data->getRows() as $row) {

				$link = $row[0];
				if (substr($link, -1) == '/') {
					$link = substr($link,0,-1);
				}
				$slug = explode('/', $link);
				$slug = array_pop($slug);
				$id = $this->get_post_by_slug($slug);
				if ($id) {

					if ($id == 784373) {
						continue;
					}
					$out[] = intval($id);
				}
			}
			if (!empty($out)) {
				update_option('telegram_realtime', $out);
			}
		}
		switch_to_blog(3);
		$data = $service->data_realtime->get( 'ga:194733755', 'rt:pageViews', $opts );
		if ( count($data->getRows()) > 0 ){

			foreach ($data->getRows() as $row) {

				$link = $row[0];
				if (substr($link, -1) == '/') {
					$link = substr($link,0,-1);
				}
				$slug = explode('/', $link);
				$slug = array_pop($slug);
				$id = $this->get_post_by_slug($slug);
				if ($id) {

					if ($id == 784373) {
						continue;
					}
					$out[] = intval($id);
				}
			}
			if (!empty($out)) {
				update_option('telegram_realtime', $out);
			}
		}
		restore_current_blog();
		}

	function get_top_themes() {
		$posts = get_option('telegram_most_read');
		$ot = array();
		$oc = array();
		foreach ($posts as $post_id) {
			$terms = wp_get_post_terms($post_id, array('post_tag', 'category'));
			foreach ($terms as $term) {
				if ($term->taxonomy == 'post_tag') {
					if ( isset( $ot[ $term->term_id ] ) ) {
						$ot[ $term->term_id ]++;
					} else {
						$ot[ $term->term_id ] = 1;
					}
				}
				else {
					$cat = $term;
					while ($cat->parent) {
						$cat = get_category($cat->parent);
					}
					$oc[$cat->term_id][] = $post_id;
				}
			}
		}
		foreach ($oc as $key=>$value) {
			set_transient('telegram_most_read_'.$key, $value);
			update_option('telegram_most_read_'.$key, $value);
			set_transient('telegram_trending_'.$key, $value);
			update_option('telegram_trending_'.$key, $value);
			$otc = array();
			foreach($value as $post_id) {
				$terms = wp_get_post_terms($post_id, array('post_tag'));
				foreach ($terms as $term) {
						if ( isset( $otc[ $term->term_id ] ) ) {
							$otc[ $term->term_id ]++;
						} else {
							$otc[ $term->term_id ] = 1;
						}

				}
			}
			arsort($otc);
			set_transient('telegram_top_teme_'.$key, array_keys($otc));
			update_option('telegram_top_teme_'.$key, array_keys($otc));
		}

		arsort($ot);
		set_transient('telegram_top_teme', array_keys($ot));
		update_option('telegram_top_teme', array_keys($ot));
	}
}
