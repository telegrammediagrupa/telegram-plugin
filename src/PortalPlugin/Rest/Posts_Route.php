<?php

namespace Telegram\PortalPlugin\Rest;

class Posts_Route extends \WP_REST_Controller {

	public function register_routes() {
		$version = '1';
		$namespace = 'tmg/telegram/v' . $version;
		$base = 'posts';
		register_rest_route( $namespace, '/' . $base, array(
			array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'get_items_permissions_check' ),
			),
		) );
		register_rest_route( $namespace, '/' . $base . '/(?P<id>[^/]+)', array(
			array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_item' ),
				'permission_callback' => array( $this, 'get_item_permissions_check' ),
				'args'                => array(
					'id' => array(
						'default' => 'view',
					),
				),
			),
		));

		register_rest_route( $namespace, '/' . $base . '/slug/(?P<slug>[^/]+)', array(
			array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_item_by_slug' ),
				'permission_callback' => array( $this, 'get_item_permissions_check' ),
				'args'                => array(
					'slug' => array(
						'default' => 'view',
					),
				),
			),
		));
	}

	/**
	 * Get a collection of items
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function get_items( $request ) {
		$items = get_posts();
		$data = array();
		foreach( $items as $item ) {
			$itemdata = $this->prepare_item_for_response( $item, $request );
			$data[] = $this->prepare_response_for_collection( $itemdata );
		}

		return new \WP_REST_Response( $data, 200 );
	}

	/**
	 * Get one item from the collection
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function get_item( $request ) {
		//get parameters from request
		$params = $request->get_params();
		$item = get_post(intval($params['id']));
		if ($item) {
			$data = $this->prepare_item_for_response( $item, $request );
			return new \WP_REST_Response( $data, 200 );
		} else {
			return new \WP_Error( 'code', __( 'message', 'text-domain' ) );
		}
	}

	public function get_item_by_slug( $request ) {
		//get parameters from request
		$params = $request->get_params();
		$item = get_post($params['slug']);
		$data = $this->prepare_item_for_response( $item, $request );

		//return a response or error based on some conditional
		if ( 1 == 1 ) {
			return new \WP_REST_Response( $data, 200 );
		} else {
			return new \WP_Error( 'code', __( 'message', 'text-domain' ) );
		}
	}

	/**
	 * Check if a given request has access to get items
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|bool
	 */
	public function get_items_permissions_check( $request ) {
		return true;
	}

	/**
	 * Check if a given request has access to get a specific item
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|bool
	 */
	public function get_item_permissions_check( $request ) {
		return $this->get_items_permissions_check( $request );
	}

	/**
	 * Prepare the item for the REST response
	 *
	 * @param mixed $item WordPress representation of the item.
	 * @param \WP_REST_Request $request Request object.
	 * @return mixed
	 */
	public function prepare_item_for_response( $item, $request ) {
		$meta = get_post_meta($item->ID);
		$sizes = get_intermediate_image_sizes();
		$images = [];
		foreach ($sizes as $size) {
			$images[$size] = get_the_post_thumbnail_url($item->ID, $size);
		}
		$images['photographer'] = get_post_meta(get_post_thumbnail_id($item->ID), 'fotograf', true);
		$authors = [];
		$coauthors = get_coauthors($item->ID);
		foreach ($coauthors as $author) {
			$authors[] = [
				'id' => $author->id,
				'display_name' => $author->display_name,
				'first_name' => $author->first_name,
				'last_name' => $author->last_name,
				'image' => get_avatar_url($author->ID, 300),
				'link' => get_author_posts_url( $author->ID, $author->user_login )
			];
		}
		$category = wp_get_post_categories($item->ID);
		return array(
			'id' => $item->ID,
			'title' => $item->post_title,
			'subtitle' => $meta['subtitle'][0],
			'content' =>  apply_filters( 'the_content', $item->post_content ),
			'overtitle' => $meta['nadnaslov'][0],
			'short_title' => $meta['short_title'][0],
			'sticker' => $meta['sticker'][0],
			'thumbnail' => $images,
			'link' => str_replace('http://staging.telegram.hr', '', get_the_permalink($item->ID)),
			'modified' => $item->post_modified,
			'recommendations' => $meta['recommendations'][0],
			'comments' => $meta['comments'][0],
			'slug' => $item->post_name,
			'authors' => $authors,
			'category' => [
				'name' => $category[0]->name,
				'link' => get_category_link($category[0])
			]
		);
	}

	/**
	 * Get the query params for collections
	 *
	 * @return array
	 */
	public function get_collection_params() {
		return array(
			'page'     => array(
				'description'       => 'Current page of the collection.',
				'type'              => 'integer',
				'default'           => 1,
				'sanitize_callback' => 'absint',
			),
			'per_page' => array(
				'description'       => 'Maximum number of items to be returned in result set.',
				'type'              => 'integer',
				'default'           => 10,
				'sanitize_callback' => 'absint',
			),
			'search'   => array(
				'description'       => 'Limit results to those matching a string.',
				'type'              => 'string',
				'sanitize_callback' => 'sanitize_text_field',
			),
		);
	}
}
