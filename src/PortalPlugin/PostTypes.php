<?php

namespace Telegram\PortalPlugin;

class PostTypes extends Instance {

	public function __construct() {
		$this->register_hook_callbacks();
	}

	public function register_hook_callbacks() {
		ActionsFilters::add_action( 'init', $this, 'register' );
		ActionsFilters::add_filter( 'rewrite_rules_array', $this,'insert_rewrite_rules' );
		//ActionsFilters::add_filter( 'fotogalerije_rewrite_rules', $this, 'add_foto_permastruct' );
		//ActionsFilters::add_filter( 'video_rewrite_rules', $this, 'add_video_permastruct' );
		ActionsFilters::add_filter( 'post_type_link', $this, 'custom_post_permalink', 2, 4 );
		ActionsFilters::add_action( 'init', $this, 'change_author_permalinks' );
		ActionsFilters::add_filter( 'get_sample_permalink', $this, 'sample_permalink', 10, 5);
		ActionsFilters::add_filter( 'msm_sitemap_entry_post_type', $this, 'sitemaps');
		ActionsFilters::add_filter('page_rewrite_rules', $this, 'page_rules');
		ActionsFilters::add_filter( '_get_page_link', $this, 'page_link', 10, 2);
		ActionsFilters::add_filter('epr_post_types', $this, 'telegram_epr_post_type');

		ActionsFilters::add_filter('coauthors_supported_post_types', $this, 'coauthor_post_types', 10, 1);
	}

	function coauthor_post_types($post_types) {
		$post_types[] = 'opportunities';
		$post_types[] = 'specijal';
		return $post_types;
	}

	function telegram_epr_post_type($array) {
		$array[] = 'partneri';
		$array[] = 'native';
		$array[] = 'opportunities';
		$array[] = 'specijal';
		return $array;
	}

	function page_link($link, $post_id) {
		return str_replace('https://www.telegram.hr/', 'https://www.telegram.hr/stranica/', $link);
	}

	function page_rules($rules) {
		$out = [];
		foreach ($rules as $key => $value) {
			$out['stranica/' . $key] = $value;
		}
		return $out;
	}

	function sitemaps($types) {
		$types[] = 'price';
		$types[] = 'video';
		$types[] = 'partneri';
		$types[] = 'native';
		$types[] = 'fotogalerije';
		$types[] = 'na-prvu';
		$types[] = 'kolumne-type';
		$types[] = 'opportunities';
		$types[] = 'specijal';
		return $types;
	}

	function sample_permalink($permalink, $post_ID, $title, $name, $post ) {
		if (strpos($permalink[0], '%category%')>-1) {
			$cats = get_the_category($post->ID);
			if ($cats) {
				$cat = $cats[0]->slug;
			}
			else {
				$cat = 'uncategorized';
			}
			$permalink[0] = str_replace('%category%', $cat, $permalink[0]);
		}
		return $permalink;
	}

	public function register() {
		if ( ! defined( 'AMP_QUERY_VAR' ) ) {
			define( 'AMP_QUERY_VAR', 'amp' );
		}
		register_post_type('fotogalerije', array(
			'label' => 'Fotogalerije',
			'public' => true,
			'menu_position' => 7,
			'menu_icon' => 'dashicons-format-gallery',
			'publicly_queryable' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'author', 'zoninator_zones', 'revisions','shortlinks' ),
			'taxonomies' => array( 'category', 'post_tag', 'author' ),
			'has_archive' => true,
			'show_in_rest' => true,
			'rewrite' => [
				'slug' => 'fotogalerije/%category%'
			]
		));
		register_post_type('video', array(
			'label' => 'Video',
			'public' => true,
			'menu_position' => 8,
			'menu_icon' => 'dashicons-format-video',
			'supports' => array( 'title', 'editor', 'thumbnail', 'author', 'zoninator_zones', AMP_QUERY_VAR,'shortlinks', 'revisions' ),
			'taxonomies' => array( 'category', 'post_tag', 'author' ),
			'has_archive' => true,
			'show_in_rest' => true,
			'rewrite' => [
				'slug' => 'video/%category%'
			]
		));
		register_post_type('price', array(
			'label' => 'Veliki format',
			'public' => true,
			'menu_position' => 6,
			'menu_icon' => 'dashicons-star-filled',
			'supports' => array( 'title', 'editor', 'thumbnail', 'author', 'zoninator_zones', AMP_QUERY_VAR,'shortlinks', 'revisions' ),
			'taxonomies' => array( 'category', 'post_tag', 'author' ),
			'has_archive' => true,
			'show_in_rest' => true
		));
		register_post_type('partneri', array(
			'label' => 'Partneri',
			'public' => true,
			'menu_position' => 9,
			'menu_icon' => 'dashicons-star-filled',
			'supports' => array( 'title', 'editor', 'thumbnail', 'author', 'zoninator_zones','shortlinks', 'revisions' ),
			'taxonomies' => array( 'category', 'post_tag', 'author' ),
			'has_archive' => false,
			'show_in_rest' => true
		));

		register_post_type('native', array(
			'label' => 'Native',
			'public' => true,
			'menu_position' => 9,
			'menu_icon' => 'dashicons-star-filled',
			'supports' => array( 'title', 'editor', 'thumbnail', 'author', 'zoninator_zones', 'revisions' ),
			'has_archive' => false,
			'show_in_rest' => true
		));

		register_post_type('opportunities', array(
			'label' => 'OS prilike',
			'public' => true,
			'menu_position' => 9,
			'menu_icon' => 'dashicons-media-spreadsheet',
			'supports' => array( 'title', 'editor', 'thumbnail', 'author', 'zoninator_zones', 'revisions' ),
			'taxonomies' => array( 'category', 'post_tag', 'author' ),
			'has_archive' => false,
			'show_in_rest' => true
		));

		register_post_type('specijal', array(
			'label' => 'Specijali',
			'public' => true,
			'menu_position' => 9,
			'menu_icon' => 'dashicons-media-spreadsheet',
			'supports' => array( 'title', 'editor', 'thumbnail', 'zoninator_zones', 'revisions' ),
			//'taxonomies' => array( 'category', 'post_tag'),
			'has_archive' => false,
			'show_in_rest' => true
		));
		register_post_type('shop-guide', array(
			'label' => 'Shop guide',
			'public' => true,
			'menu_position' => 9,
			'menu_icon' => 'dashicons-media-spreadsheet',
			'supports' => array( 'title', 'editor', 'thumbnail', 'zoninator_zones', 'revisions', 'page-attributes' ),
			//'taxonomies' => array( 'category', 'post_tag'),
			'has_archive' => false,
			'show_in_rest' => true,
			'hierarchical' => true,
			'rewrite' => [
				'slug' => 'soping-vodic'
			]
		));
	}

	function custom_post_permalink( $permalink, $post, $leavename, $sample ) {

		// only do our stuff if we're using pretty permalinks
		// and if it's our target post type
		if ( $post->post_status == 'publish' && ($post->post_type == 'fotogalerije' || $post->post_type=='video' ) && get_option( 'permalink_structure' ) ) {

			// remember our desired permalink structure here
			// we need to generate the equivalent with real data
			// to match the rewrite rules set up from before

			$rewritecodes = array(
				'%category%'
			);

			// setup data
			//$terms = get_the_terms($post->ID, 'category');
			//$unixtime = strtotime( $post->post_date );

			// this code is from get_permalink()
			$category = '';
			//if ( strpos($permalink, '%category%') !== false ) {

			$cats = get_the_category($post->ID);
			if ( $cats ) {
				usort($cats, '_usort_terms_by_ID'); // order by ID
				$category = $cats[0]->slug;
				if ( $parent = $cats[0]->parent )
					$category = get_category_parents($parent, false, '/', true) . $category;
			}
			// show default category in permalinks, without
			// having to assign it explicitly
			if ( empty($category) ) {
				$default_category = get_category( get_option( 'default_category' ) );
				$category = is_wp_error( $default_category ) ? '' : $default_category->slug;
			}
			//}


			$replacements = array(
				$category
			);

			// finish off the permalink
			$permalink = str_replace( $rewritecodes, $replacements, $permalink );
			$permalink = user_trailingslashit($permalink, 'single');
		}

		return $permalink;
	}

	function change_author_permalinks() {

		global $wp_rewrite;

		// Change the value of the author permalink base to whatever you want here
		$wp_rewrite->author_base = 'autor';
	}

	function add_foto_permastruct( $rules ) {
		global $wp_rewrite;

		// set your desired permalink structure here
		$struct = '/fotogalerije/%category%/%postname%/';

		// use the WP rewrite rule generating function
		$rules = $wp_rewrite->generate_rewrite_rules(
			$struct,       // the permalink structure
			EP_PERMALINK,  // Endpoint mask: adds rewrite rules for single post endpoints like comments pages etc...
			false,         // Paged: add rewrite rules for paging eg. for archives (not needed here)
			true,          // Feed: add rewrite rules for feed endpoints
			true,          // For comments: whether the feed rules should be for post comments - on a singular page adds endpoints for comments feed
			false,         // Walk directories: whether to generate rules for each segment of the permastruct delimited by '/'. Always set to false otherwise custom rewrite rules will be too greedy, they appear at the top of the rules
			true           // Add custom endpoints
		);

		return $rules;
	}

	function add_video_permastruct( $rules ) {
		global $wp_rewrite;

		// set your desired permalink structure here
		$struct = '/video/%category%/%postname%/';

		// use the WP rewrite rule generating function
		$rules = $wp_rewrite->generate_rewrite_rules(
			$struct,       // the permalink structure
			EP_PERMALINK,  // Endpoint mask: adds rewrite rules for single post endpoints like comments pages etc...
			false,         // Paged: add rewrite rules for paging eg. for archives (not needed here)
			true,          // Feed: add rewrite rules for feed endpoints
			true,          // For comments: whether the feed rules should be for post comments - on a singular page adds endpoints for comments feed
			false,         // Walk directories: whether to generate rules for each segment of the permastruct delimited by '/'. Always set to false otherwise custom rewrite rules will be too greedy, they appear at the top of the rules
			true           // Add custom endpoints
		);

		return $rules;
	}

	function insert_rewrite_rules( $rules ) {
		$newrules = array();

		$newrules['(fotogalerije|video)/(.+?)/([^/]+)/?$'] = 'index.php?post_type=$matches[1]&category_name=$matches[2]&name=$matches[3]';
		$newrules['(fotogalerije|video)/(.+?)/?$'] = 'index.php?post_type=$matches[1]&category_name=$matches[2]';
		return $newrules + $rules;
	}
}