<?php
namespace Telegram\PortalPlugin;

class Feeds extends Instance {

    public function __construct() {
        $this->register_hook_callbacks();
    }

	public function register_hook_callbacks() {
		add_action('init', array($this, 'add_feeds'));
		add_action( 'rss2_ns', [$this, 'namespace'] );
		add_filter( 'the_excerpt_rss', [$this, 'excerpt'] );
	}

	function namespace() {
        echo ' xmlns:media="http://search.yahoo.com/mrss/" ';
    }

    function excerpt($output) {
        $output = get_field('subtitle');
        return $output;
    }

	function add_feeds() {
		add_feed('tes', array($this, 'tes_feed'));
		add_feed('nethr', array($this, 'nethr_feed'));
		add_feed('midas', array($this, 'midas_feed'));
		add_feed('esp/most_read', array($this, 'najcitanije'));
		add_feed('esp/important', array($this, 'important'));
		add_feed('esp/zivot', [$this, 'zivot']);
		add_feed('esp/biznis-tech', [$this, 'biznis_tech']);
		add_feed('esp/komentari', [$this, 'komentari']);
		add_feed('esp/politika-kriminal', [$this, 'politika']);
		add_feed('esp/kultura', [$this, 'kultura']);
		add_feed('esp/velike-price', [$this, 'velike']);

		add_feed('linker', [$this, 'linker']);
	}

	function linker() {
		$q = new \WP_Query([
			'posts_per_page' => 10,
			'post_type' => 'any',
			'post_status' => 'publish',
			'no_found_rows' => true,
			'category_name' => get_queried_object()->slug,
		]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/linker</link>
                <description/>
                <atom:link href="https://www.telegram.hr/most_read" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					$this->item('full');
				} ?>
            </channel>
        </rss>
		<?php
    }

	function tes_feed() {
		$q = new \WP_Query(array(
			'posts_per_page' => 10,
			'post__in' => get_option('telegram_most_read'),
			'orderby' => 'post__in',
			'post_type' => array('post', 'price', 'fotogalerije', 'video'),
			'post_status' => 'publish',
			'no_found_rows' => true
		));
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
		<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
			<channel>
				<title>Telegram.hr</title>
				<link>https://www.telegram.hr/tes</link>
				<description/>
				<atom:link href="https://www.telegram.hr/tes" rel="self"/>
				<language>hr-HR</language>
				<lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
				?>
				<item>
					<title><?php the_title(); ?></title>
					<link><?php the_permalink(); ?></link>
					<description><![CDATA[<?php echo get_the_excerpt(); ?>]]></description>
					<guid><?php the_permalink(); ?></guid>
					<enclosure url="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" length="0" type="None"/>
				</item>
			<?php } ?>
				</channel>
			</rss>
<?php
	}

	function nethr_feed() {
		$q = new \WP_Query(array(
			'posts_per_page' => 10,
			'post__in' => get_option('telegram_realtime'),
			'orderby' => 'post__in',
			'post_type' => array('post', 'price'),
			'post_status' => 'publish',
			'no_found_rows' => true
		));
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?><rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
			<channel>
				<title>Telegram.hr</title>
				<link>https://www.telegram.hr/nethr</link>
				<description/>
				<atom:link href="https://www.telegram.hr/nethr" rel="self"/>
				<language>hr-HR</language>
				<lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					if ( get_the_ID() == 379482) {
						continue;
					}
					?>
					<item>
						<title><?php the_title(); ?></title>
						<link><?php the_permalink(); ?></link>
						<description><![CDATA[<?php echo get_the_excerpt(); ?>]]></description>
						<guid><?php the_permalink(); ?></guid>
						<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'article-1');
						?>
						<enclosure url="<?php echo $image[0] ?>" length="None" type="None"/>
					</item>
				<?php } ?>
			</channel>
		</rss>
		<?php
	}

	function midas_feed() {
		$q = new \WP_Query(array(
			'posts_per_page' => 10,
			'post_type' => array('post', 'price', 'fotogalerije', 'video'),
			'post_status' => 'publish',
			'no_found_rows' => true,
            'ignore_sticky_posts' => true,
            'category__not_in' => [3926]
		));
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?><rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
        <channel>
            <title>Telegram.hr</title>
            <link>https://www.telegram.hr/midas</link>
            <description/>
            <atom:link href="https://www.telegram.hr/midas" rel="self"/>
            <language>hr-HR</language>
            <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
			<?php while ($q->have_posts()) {
				$q->the_post();
				if ( get_the_ID() == 379482) {
					continue;
				}
				?>
                <item>
                    <title><?php the_title(); ?></title>
                    <link><?php the_permalink(); ?></link>
                    <description><![CDATA[<?php echo get_the_excerpt(); ?>]]></description>
                    <guid><?php the_permalink(); ?></guid>
					<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'article-1');
					?>
                    <enclosure url="<?php echo $image[0] ?>" length="0" type="None"/>
                </item>
			<?php } ?>
        </channel>
        </rss>
		<?php
	}

	function najcitanije() {
		$q = new \WP_Query([
		        'posts_per_page' => 10,
		        'post_status' => 'publish',
		        'no_found_rows' => true,
		        'post__in'            => get_option( 'telegram_cxense_most_read' ),
		        'post_type'           => array( 'post', 'price' ),
                'orderby' => 'post__in'
        ]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/esp/most_read</link>
                <description/>
                <atom:link href="https://www.telegram.hr/most_read" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
				    $this->item();
				} ?>
            </channel>
        </rss>
		<?php
	}

	function important() {
		$q = new \WP_Query([
		        'posts_per_page' => 10,
            'post_type' => 'any',
            'no_found_rows' => true,
            'meta_key' => '_zoninator_order_37783',
            'orderby' => 'meta_value_num',
            'date_query' => [
                [
                        'hour' => 17,
                    'compare' => '>='
                ],
                [
                        'day' => date('D', strtotime('yesterday'))
                ]
            ]
        ]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/esp/important</link>
                <description/>
                <atom:link href="https://www.telegram.hr/esp/important" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					$this->item();
					 } ?>
            </channel>
        </rss>
		<?php
	}

	function biznis_tech() {
		$q = new \WP_Query([
			'posts_per_page' => 10,
			'post_type' => 'any',
			'post_status' => 'publish',
			'no_found_rows' => true,
            'category_name' => 'biznis-tech',
			'tax_query' => [
				[
					'taxonomy' => 'author',
					'field' => 'term_id',
					'terms' => [37857],
					'operator' => 'NOT IN'
				]
			]
		]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/esp/most_read</link>
                <description/>
                <atom:link href="https://www.telegram.hr/most_read" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					$this->item();
				} ?>
            </channel>
        </rss>
		<?php
	}

	function zivot() {
		$q = new \WP_Query([
			'posts_per_page' => 10,
			'post_type' => 'any',
			'post_status' => 'publish',
			'no_found_rows' => true,
            'category_name' => 'zivot',
			'tax_query' => [
				[
					'taxonomy' => 'author',
					'field' => 'term_id',
					'terms' => [37857],
					'operator' => 'NOT IN'
				]
			]
		]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/esp/most_read</link>
                <description/>
                <atom:link href="https://www.telegram.hr/most_read" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					$this->item();
				} ?>
            </channel>
        </rss>
		<?php
	}

	function politika() {
		$q = new \WP_Query([
			'posts_per_page' => 10,
			'post_type' => 'any',
			'post_status' => 'publish',
			'no_found_rows' => true,
			'category_name' => 'politika-kriminal',
			'tax_query' => [
				[
					'taxonomy' => 'author',
					'field' => 'term_id',
					'terms' => [37857],
					'operator' => 'NOT IN'
				]
			]
		]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/esp/most_read</link>
                <description/>
                <atom:link href="https://www.telegram.hr/most_read" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					$this->item();
				} ?>
            </channel>
        </rss>
		<?php
	}

	function kultura() {
		$q = new \WP_Query([
			'posts_per_page' => 10,
			'post_type' => 'any',
			'post_status' => 'publish',
			'no_found_rows' => true,
			'category_name' => 'kultura',
			'tax_query' => [
				[
					'taxonomy' => 'author',
					'field' => 'term_id',
					'terms' => [37857],
					'operator' => 'NOT IN'
				]
			]
		]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/esp/most_read</link>
                <description/>
                <atom:link href="https://www.telegram.hr/most_read" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					$this->item();
				} ?>
            </channel>
        </rss>
		<?php
	}

	function velike() {
		$q = new \WP_Query([
			'posts_per_page' => 10,
			'post_type' => 'any',
			'post_status' => 'publish',
			'no_found_rows' => true,
			'category_name' => 'velike-price'
		]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/esp/most_read</link>
                <description/>
                <atom:link href="https://www.telegram.hr/most_read" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					$this->item();
				} ?>
            </channel>
        </rss>
		<?php
	}

	function komentari() {
		$q = new \WP_Query([
			'posts_per_page' => 10,
			'post_type' => 'any',
			'post_status' => 'publish',
			'no_found_rows' => true,
            'category_name' => 'komentari',
		]);
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
        <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/">
            <channel>
                <title>Telegram.hr</title>
                <link>https://www.telegram.hr/esp/most_read</link>
                <description/>
                <atom:link href="https://www.telegram.hr/most_read" rel="self"/>
                <language>hr-HR</language>
                <lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while ($q->have_posts()) {
					$q->the_post();
					$this->item();
				} ?>
            </channel>
        </rss>
		<?php
    }

	function item($size = 'thumbnail') {
        ?><item>
        <title><?php the_title(); ?></title>
        <link><?php the_permalink(); ?></link>
        <description><![CDATA[<?php (isset($_GET['espf']) && sanitize_text_field($_GET['espf']) === '1312')?the_content():the_field('subtitle'); ?>]]></description>
        <guid><?php the_permalink(); ?></guid>
        <category><![CDATA[<?php echo $cat = get_the_category()[0]->name ?>]]></category>
        <overtitle><![CDATA[<?php echo get_post_meta(get_the_ID(), 'nadnaslov', true)?:$cat ?>]]></overtitle>
		<?php
		$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
		if ($size === 'full') {
		    $url = get_the_post_thumbnail_url(get_the_ID(), 'full');
		    $url = $this->signImage($url);
		} else {
			$thumbnail = image_get_intermediate_size( $thumbnail_id );
			$url       = $thumbnail['url'];
		}
		if ( $url ) {
			?>
            <media:content height="150" width="150" url="<?php echo $url ?>" medium="image" />
            <media:credit><?php echo telegram_get_photographer( $thumbnail_id ) ?></media:credit>
            <media:description><?php echo get_the_post_thumbnail_caption( get_the_ID() ) ?></media:description>
		<?php }
		?>
        </item><?php
	}

	private function signImage($url, $preset = 'single1') {
		if (!$url) {
			return '';
		}
		$key = 'b5b54148b47074a162ff4bd3323353b1ad11401d84159bb8d2d4dc855a2145b3';
		$salt = '2eec61e5608da13beb3b8002deb178c0437be3b22b9ed38427cdeade6875d53e';

		$keyBin = pack("H*" , $key);
		if(empty($keyBin)) {
			die('Key expected to be hex-encoded string');
		}

		$saltBin = pack("H*" , $salt);
		if(empty($saltBin)) {
			die('Salt expected to be hex-encoded string');
		}

		$extension = 'jpg';

		if (strpos($url, '.gif')) {
			$extension = 'gif';
			return $url;
		}

		$encodedUrl = rtrim(strtr(base64_encode($url), '+/', '-_'), '=');

		$path = "/preset:{$preset}/{$encodedUrl}.{$extension}";

		$signature = rtrim(strtr(base64_encode(hash_hmac('sha256', $saltBin.$path, $keyBin, true)), '+/', '-_'), '=');

		return sprintf("https://images.telegram.hr/%s%s", $signature, $path);
	}
}