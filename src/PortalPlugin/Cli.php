<?php

namespace Telegram\PortalPlugin;

class Cli extends \WP_CLI_Command {

	private static $_instance = null;

	public function __construct() {
		\WP_CLI::add_command( 'telegram_plugin', $this );
	}

	public static function instance () {
		$classname = get_called_class();
		if ( is_null( self::$_instance ) )
			self::$_instance = new $classname();
		return self::$_instance;
	}

	public function realtime($args, $assoc_args) {
		$google = new Google();
		$google->get_realtime();
	}

	public function most_read($args, $assoc_args) {
		$google = new Google();
		$google->get_most_read();
	}

	public function count_user_posts($args, $assoc_args) {
		$authors = new Authors();
		$authors->generate_stats();
	}

	public function regenerate_avatars($args, $assoc_args) {
		$q = new \WP_Query([
			'posts_per_page' => -1,
			'post_type' => 'guest-author',
			'post_status' => 'publish',
			'no_found_rows' => true,
			'ignore_sticky_posts' => true
		]);
		$ids = [];
		while ($q->have_posts()) {
			$q->the_post();
			$ids[] = get_post_thumbnail_id();
		}
		$ids = implode(' ', $ids);
		\WP_CLI::runcommand('media regenerate '.$ids. ' --yes');
	}

	public function get_zip() {
		$podaci = [
			1 => [
				'name' => 'nov018',
				'pass' => 'eFvu8W27UjHLiYw150'
			],
			2 => [
				'name' => 'nov019',
				'pass' => 'lfLZ98AcmZsTEdkUCd'
			],
			3 => [
				'name' => 'nov020',
				'pass' => 'yudMRmsUGQUoQCLOXE'
			],
		];


		$headers = [
			'Authorization' => 'Basic ' . base64_encode( $podaci[2]['name'] . ':' . $podaci[2]['pass'] )
		];
		$file_data = wp_remote_get('https://www.apis-it.hr:8443/izbori/', [
			'headers' => $headers
		]);
		$has_file = false;
		if (!is_wp_error($file_data)) {
			\WP_CLI::line('url works');
			$dom = new \DOMDocument();
			$dom->loadHTML( $file_data['body'] );
			$files = $dom->getElementsByTagName( 'a' );
			$file  = $files[ sizeof( $files ) - 1 ];
			if ($file) {
				\WP_CLI::line('we have files');
				// Use wp_remote_get to fetch the data
				$file_data = wp_remote_get( 'https://www.apis-it.hr:8443/izbori/' . $file->getAttribute( 'href' ), [
					'headers' => $headers
				] );
				if (!is_wp_error($file_data)) {
					\WP_CLI::line('file download');
					$zip = $file_data['body'];

					$file = dirname( __FILE__ ) . '/file1.zip';
					$this->filesystem();
					global $wp_filesystem;
					$wp_filesystem->put_contents( $file, $zip );
					$has_file = true;
				}
			}
		}

		$data = [
			'total' => [],
			'counted' => '',
			'age' => ''
		];
		$z = new \ZipArchive();

		$total_votes = 0;
		$map = [
			'SDP, HSS, HSU, SNAGA, GLAS, IDS, PGS' => 'restart',
			'SDP, IDS, PGS, HSS, HSU, SNAGA, GLAS' => 'restart',
			'SDP, HSS, HSU, SNAGA, GLAS, IDS, PGS, REFORMISTI' => 'restart',
			'HDZ' => 'hdz',
			'HDZ, HSLS' => 'hdz',
			'HDZ, HDS' => 'hdz',
			'MOŽEMO! - POLITIČKA PLATFORMA, ZAGREB JE NAŠ!, NL, RF, ORAH, ZA GRAD' => 'mozemo',
			'MOŽEMO! - POLITIČKA PLATFORMA, RF, NL, ORAH, ZAGREB JE NAŠ!, ZA GRAD' => 'mozemo',
			'MOŽEMO! - POLITIČKA PLATFORMA, NL, RF, ORAH, ZAGREB JE NAŠ!, ZA GRAD' => 'mozemo',
			'DOMOVINSKI POKRET, HRVATSKI SUVERENISTI, BLOK, HKS, HRAST - POKRET ZA USPJEŠNU HRVATSKU, SU, ZELENA LISTA' => 'domovinski',
			'IP, PAMETNO, FOKUS' => 'simp',
			'ŽELJKO KERUM - HRVATSKA GRAĐANSKA STRANKA' => 'kerum',
			'NLSP, AM, ŽIVI ZID, PH, SIP' => 'dosta',
			'ŽIVI ZID, PH, HSS - SR, SIP, HSSČKŠ, ZSZ, NLSP' => 'dosta',
			'ŽIVI ZID, SIP, PH, HSS - SR' => 'dosta',
			'REFORMISTI, HSS BRAĆE RADIĆ, UMIROVLJENICI' => 'reformisti',
			'ŽELJKO LACKOVIĆ - NEZAVISNE LISTE, REFORMISTI, HSS BRAĆE RADIĆ, NSH, HDS' => 'reformisti',
			'REFORMISTI' => 'reformisti',
			'MOST' => 'most',
			'"365 STRANKA RADA I SOLIDARNOSTI"' => '365',
			'HNS' => 'hns',
			'HNS, HSS BRAĆE RADIĆ' => 'hns',
		];
		if ($has_file) {
			\WP_CLI::line('process new file');
			//get the data from jsons
			if ($z->open(dirname(__FILE__ ) .'/file1.zip')) {
				$contents = '';
				$fp = $z->getStream("r_00_000_0000_000.json" );
				if ( ! $fp ) {
					exit( "failed\n" );
				}

				while ( ! feof( $fp ) ) {
					$contents .= fread( $fp, 2 );
				}
				$contents = \json_decode($contents, true);
				$data['counted'] = $contents['sumarniStatus']['bmPosto'];
				$data['age'] = $contents['sumarniStatus']['vrijeme'];
				for ($i = 1; $i < 12; $i++) {
					$data[$i] = [];
					$contents = '';
					$fp = $z->getStream( sprintf("r_02_%03d_0000_000.json", $i) );
					if ( ! $fp ) {
						exit( "failed\n" );
					}

					while ( ! feof( $fp ) ) {
						$contents .= fread( $fp, 2 );
					}
					$contents = \json_decode($contents, true);
					$data[$i]['counted'] = round($contents['bmZatvoreno']/$contents['bmUkupno'] * 100,2);
					$total = $contents['listiciVazeci'];
					$total_votes += $total;
					$out = [];
					foreach ($contents['lista'] as $party) {
						if (isset($map[$party['naziv']])) {
							$key = $map[ $party['naziv'] ];
						}
						else {
							$key = $party['naziv'];
						}
						$votes = $party['glasova'];
						if (isset($data['total'][$key]['votes'])) {
							$data['total'][ $key ]['votes'] += $votes;
						}
						else {
							$data['total'][$key] = [];
							$data['total'][ $key ]['votes'] = $votes;
						}
						$data[$i]['party'][$key] = [
							'postotak' => $party['posto']
						];
						if ($votes/$total >= 0.05) {
							$out[$key] = $party['glasova'];
						}
					}
					$arr = array_count_values(
						iterator_to_array(
							new \LimitIterator( $this->gen_dhondt($out), 0, $i==11?3:14)
						)
					);
					foreach ($arr as $party => $mandati) {
						$data[$i]['party'][$party]['mandati'] = $mandati;
						if ( isset( $data['total'][ $party ]['mandati'] ) ) {
							$data['total'][ $party ]['mandati'] += $mandati;
						} else {
							$data['total'][ $party ]['mandati'] = $mandati;
						}
					}
				}
				fclose($fp);
			}
			foreach ($data['total'] as &$party) {
				$party['postotak'] = round($party['votes']/$total_votes*100, 2);
			}
			$data['dip'] = true;
			\WP_CLI::line('update_option');
			// process each to determine number of seats
			update_option('tmg_izbori_2020_total', $data);
		}/*
		else {
			\WP_CLI::line('process_old_file');
			//get the data from jsons
			if ( $z->open( dirname( __FILE__ ) . '/file.zip' ) ) {
				$data['dip'] = false;
				$data['counted'] = 0;
				$data['age']     = '19:00';
				for ( $i = 1; $i < 12; $i ++ ) {
					$data[ $i ] = [];
					$contents   = '';
					$fp         = $z->getStream( sprintf( "r_02_%03d_0000_000.json", $i ) );
					if ( ! $fp ) {
						exit( "failed\n" );
					}

					while ( ! feof( $fp ) ) {
						$contents .= fread( $fp, 2 );
					}
					$contents              = \json_decode( $contents, true );
					$data[ $i ]['counted'] = 0;
					$total                 = 0;
					$total_votes           += $total;
					foreach ( $contents['lista'] as $party ) {
						if ( isset( $map[ $party['naziv'] ] ) ) {
							$key = $map[ $party['naziv'] ];
						} else {
							$key = $party['naziv'];
						}
							$data[ $i ]['party'][ $key ]['mandati'] = 0;
							$data[ $i ]['party'][ $key ] = [ 'postotak' => 0 ];
					}
				}
				fclose( $fp );
			}
			\WP_CLI::line('process page meta');
			$rows = get_field('koalicije', 917728);
			$has_mandate = false;
			if( $rows ) {
				foreach( $rows as $row ) {
					\WP_CLI::line($row['key']);
					$data['total'][$row['key']] = [];
					$data['total'][$row['key']]['postotak'] = $row['postotak'];
					if ($row['mandati'] !== '') {
						$data['total'][ $row['key'] ]['mandati'] = $row['mandati'];
						$has_mandate = true;
					}
				}
			}
			if (!$has_mandate) {
				$out = [];
				foreach ($data['total'] as $party => $values) {
					if ($values['postotak']>=5) {
						$out[$party] = $values['postotak'] * 100;
					}
				}
				$arr = array_count_values(
					iterator_to_array(
						new \LimitIterator( $this->gen_dhondt($out), 0, 143)
					)
				);
				foreach ($arr as $party => $mandati) {
					$data['total'][ $party ]['mandati'] = $mandati;
				}
			}

		}*/
	}

	function gen_dhondt(array $results) {
		// using SplPriorityQueue as a sorted listed
		$foo = new \SplPriorityQueue;
		$foo->setExtractFlags(\SplPriorityQueue::EXTR_DATA);
		// initialize the list
		foreach( $results as $party=>$votes ) {
			$foo->insert(['name'=>$party, 'div'=>1, 'votes'=>$votes], $votes);
		}

		// get the element with the highest votes/divisor
		foreach( $foo as $next ) {
			// re-enqueue this element with ++divisor
			++$next['div'];
			$foo->insert($next, $next['votes']/$next['div']);

			// "return" the name of this element
			yield $next['name'];
		}
	}

	function filesystem()
	{
		global $wp_filesystem;

		if ( is_null( $wp_filesystem ) ) {
			require_once ABSPATH . '/wp-admin/includes/file.php';
			WP_Filesystem();
		}

		return $wp_filesystem;
	}

	public function cx_most_read() {
		$c = new Cxense();
		$c->getTraffic();
	}

	public function get_sheet() {
		$c = new Sheets();
		$c->getSheetData();
	}
}