<?php
namespace Telegram\PortalPlugin;

class Analytics extends Instance {

	public function __construct() {
		$this->register_hook_callbacks();
	}

	public function register_hook_callbacks() {
		ActionsFilters::add_action( 'wp_head', $this, 'gemius' );
		//ActionsFilters::add_action( 'wp_head', $this, 'gde' );
		ActionsFilters::add_action( 'wp_head', $this, 'ga' );
		ActionsFilters::add_action( 'wp_head', $this, 'impressions' );
		ActionsFilters::add_action( 'telegram_body_start', $this, 'body_start' );
		ActionsFilters::add_action( 'telegram_body_start', $this, 'midas' );
		ActionsFilters::add_filter( 'amp_post_template_analytics', $this,'amp_add_custom_analytics', 10, 1);
		ActionsFilters::add_action('amp_post_template_footer', $this, 'dotmetrics_amp', 10, 1);
    }

	function gemius() {
		$id     = 'nSblbvtw7YnzUiC8AtarvJdS3yggumM2F_xjEZ.9W1..57';
		$cat    = false;
		$dot_id = 1182;
		if ( is_home() ) {
			$dot_id = '1173';
		}
		if ( is_single() ) {
		    $cats = get_the_category();
		    if (count($cats)) {
			    $cat = $cats[0];
			    while ( $cat->parent ) {
				    $cat = get_category( $cat->parent );
			    }
		    }
		    else {
		        $cat = false;
            }
		}
		if ( is_category() ) {
			$cat = get_category( get_query_var( 'cat' ) );
			while ( $cat->parent ) {
				$cat = get_category( $cat->parent );
			}
		}
		if ( $cat ) {
			switch ( $cat->slug ) {
				case 'politika-kriminal':
					$id     = 'B8LrC2M_SDgyAhNNdFR0m7bvPzHwbSNkjTfp3wsMi9T.T7';
					$dot_id = '1174';
					break;
				case 'biznis-tech':
					$id     = 'B8LlDWM_7dM4HvpilXmwCLbv353wbWMat5lj3jhrA2z.H7';
					$dot_id = '1176';
					break;
				case 'kultura':
					$id     = 'nczrCvuiSDiTTzjVqj3v_JeAfXAgAzrQTPxnBVuL3iv.d7';
					$dot_id = '1178';
					break;
				case 'sport':
					$id     = 'nSdLaPtwWDjz8rNOgspcbpbyP_ggyiNot0lj3JiKi6H.G7';
					$dot_id = '1175';
					break;
				case 'velike-price':
					$id     = 'nSbrCPtwSAGdwXMWSgCUMuVzP1IdASN00odjESlss0D.j7';
					$dot_id = '1177';
					break;
				case 'zivot':
					$id     = 'nSdLyvtwW.A9i9jJKh9.c_UTHZEdEXuOnL7hjdYBhsX.h7';
					$dot_id = '1179';
					break;
			}
		}
		?>
        <!-- (C)2000-2015 Gemius SA - gemiusAudience / telegram.hr / Naslovnica -->
        <script type="text/javascript">
            var pp_gemius_identifier = '<?php echo $id; ?>';
            var tmg_gemius_identifier = '<?php echo $id; ?>';
            // lines below shouldn't be edited
            function gemius_pending(i) {
                window[i] = window[i] || function () {
                        var x = window[i + '_pdata'] = window[i + '_pdata'] || [];
                        x[x.length] = arguments;
                    };
            };
            gemius_pending('gemius_hit');
            gemius_pending('gemius_event');
            gemius_pending('pp_gemius_hit');
            gemius_pending('pp_gemius_event');
            (function (d, t) {
                try {
                    var gt = d.createElement(t), s = d.getElementsByTagName(t)[0], l = 'http' + ((location.protocol == 'https:') ? 's' : '');
                    gt.setAttribute('async', 'async');
                    gt.setAttribute('defer', 'defer');
                    gt.src = l + '://hr.hit.gemius.pl/xgemius.js';
                    s.parentNode.insertBefore(gt, s);
                } catch (e) {
                }
            })(document, 'script');
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            (function() {
                window.dm=window.dm||{AjaxData:[]};
                window.dm.AjaxEvent=function(et,d,ssid,ad){
                    dm.AjaxData.push({et:et,d:d,ssid:ssid,ad:ad});
                    window.DotMetricsObj&&DotMetricsObj.onAjaxDataUpdate();
                };
                var d=document,
                    h=d.getElementsByTagName('head')[0],
                    s=d.createElement('script');
                s.type='text/javascript';
                s.async=true;
                s.src=document.location.protocol + '//script.dotmetrics.net/door.js?id=<?php echo $dot_id; ?>';
                h.appendChild(s);
            }());
            /* ]]> */
        </script>
		<?php
	}

	function gde() {
		?>
        <script>
            (function () {
                var s = new Date().getTime();
                (new Image()).src = "//gders.hit.gemius.pl/_" + s + "redot.gif?id=ndBF1DssWWTfzjfiO3OOfHXpTBN1p3gVkEJninwFwHj.h7/fastid=hsgdogucskyizeldmiugjajsfxld/stparam=pgpicqlthj";
            })();
        </script>
		<?php
	}

	function ga() { ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', {
                trackingId: 'UA-60611577-1',
                cookieDomain: 'auto',
                useAmpClientId: true,
                cookieFlags: 'SameSite=None; Secure'
            });
            ga('require', 'displayfeatures');
            ga('require', 'linkid', 'linkid.js');
            ga('set', 'anonymizeIp', true);
            ga('send', 'pageview');

        </script>
	<?php
	}

	function amp_add_custom_analytics( $analytics ) {
		if ( ! is_array( $analytics ) ) {
			$analytics = array();
		}

		$analytics['telegram-facebook'] = array(
			'type' => 'facebookpixel',
			'attributes' => array(
			),
			'config_data' => array(
                'vars' => [
                    'pixelId' => '1132408460152629'
                ],
                'triggers' => array(
	                'trackPageview' => array(
		                'on' => 'visible',
		                'request' => 'pageview',
	                ),
                ),
            )
		);

		$analytics['telegram-googleanalytics'] = array(
			'type' => 'googleanalytics',
			'attributes' => array(
				// 'data-credentials' => 'include',
			),
			'config_data' => array(
				'vars' => array(
					'account' => "UA-60611577-1"
				),
				'triggers' => array(
					'trackPageview' => array(
						'on' => 'visible',
						'request' => 'pageview',
					),
				),
			),
		);
		return $analytics;
	}

	function body_start() {
		?>
       <script>
           window.fbAsyncInit = function() {
               FB.init({
                   appId      : '1383786971938581',
                   xfbml      : true,
                   version    : 'v10.0'
               });
               FB.AppEvents.logPageView();
           };

           (function(d, s, id){
               var js, fjs = d.getElementsByTagName(s)[0];
               if (d.getElementById(id)) {return;}
               js = d.createElement(s); js.id = id;
               js.src = "https://connect.facebook.net/hr_HR/sdk.js";
               fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
       </script>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/hr_HR/fbevents.js');
            fbq('init', '1132408460152629');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none"
                 src="https://www.facebook.com/tr?id=1132408460152629&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->


        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NJZDXW"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-NJZDXW');</script>
        <!-- End Google Tag Manager -->
       <?php
	}

	function dotmetrics_amp() {
		$id = '1182';
		$cat = false;
        $special = false;
		if ( is_single() ) {
            $categories = get_the_category();

            foreach ($categories as $category) {
                $cat = $category;
	            if ( $cat->parent ) {
		            $cat = get_category( $cat->parent );
	            }
                if (in_array($cat->slug, ['openspace', 'pitanje-zdravlja'])) {
                    $special = true;
                    break;
                }
            }
			if (!$special) {
                $cat = $categories[0];
				if ( $cat->parent ) {
					$cat = get_category( $cat->parent );
				}
			}
		}
		if ( is_category() ) {
			$cat = get_category( get_query_var( 'cat' ) );
			while ( $cat->parent ) {
				$cat = get_category( $cat->parent );
			}
		}
		if ( $cat ) {
			switch ( $cat->slug ) {
				case 'politika-kriminal':
					$id     = '1174';
					break;
				case 'biznis-tech':
					$id     = '1176';
					break;
				case 'kultura':
					$id     = '1178';
					break;
				case 'sport':
					$id     = '1175';
					break;
				case 'velike-price':
					$id     = '1177';
					break;
				case 'zivot':
					$id     = '1179';
					break;
                case 'openspace':
                    $id = '12608';
                    break;
                case 'pitanje-zdravlja':
                    $id = '12607';
                    break;
			}
		}

		?>
        <amp-analytics config="https://script.dotmetrics.net/AmpConfig.json?id=<?php echo $id ?>"></amp-analytics>
        <amp-analytics  config="https://gahr.hit.gemius.pl/amp.config.json">
			<?php $id = 'bOs7KRhK_A1yEB4VXlnR.vUHfeAdITsw3Ii0_oqoKm..17';
			if ( $cat ) {
				switch ( $cat->slug ) {
					case 'politika-kriminal':
						$id     = '0iWbi693CEYXx_ha_wzeSqdz33gg42OGV66Q3MPED4D.e7';
						break;
					case 'biznis-tech':
						$id     = 'bIs7yxfa_MQcRh4HXjHZhfUHP8IdISLUEgE2RATvp9r.P7';
						break;
					case 'kultura':
						$id     = 'bIGV7xhNrZgSy8_DzkKSPadFHZcgmnrijPi0DGdZ_nL.c7';
						break;
					case 'sport':
						$id     = 'bIGVTxhNrRgSK88TTpYa3acTHQEgI3ru7TM_No9cJdv.97';
						break;
					case 'velike-price':
						$id     = 'bIGVTRhNreESZYWsTtBegfUH33gdIWOGEgE25C17T0n.L7';
						break;
					case 'zivot':
						$id     = 'bIE7KRhN_A1ycL3oXfNJf6blPzgg2iO47XE2rw_C.1n.P7';
						break;
				}
			} ?>
            <script type="application/json">
        {
            "vars": {
                "identifier": "<?php echo $id; ?>"
            }
        }
    </script>
        </amp-analytics>
        <amp-analytics type="gtag" data-credentials="include">
            <script type="application/json">
                {
                    "vars" : {
                        "gtag_id": "255998648",
                        "config" : {
                            "255998648": { "groups": "default" }
                        }
                    }
                }
            </script>
        </amp-analytics>
        <amp-analytics type="cxense">
            <script type="application/json">
                {
                    "vars": {
                        "siteId": "1128464677385494954"
                    }
                }
            </script>
        </amp-analytics>
        <?php
    }

    function impressions() {
	    if (is_single()) {
	        the_field('impression_kodovi'); 
        }
    }

    function midas() {
	    ?>
        <!-- Midas Pixel Code -->
        <img height="1" width="1" style="display:none" src="https://cdn.midas-network.com/MidasPixel/IndexAsync/17d0033e-5183-496d-a041-d04d1faf0552" />
        <!-- DO NOT MODIFY -->
        <!-- End Midas Pixel Code -->
        <?php
    }
}

