<?php

namespace Telegram\PortalPlugin;

use Telegram\PortalPlugin\Rest\Posts_Route;

class Rest extends Instance {

	/**
	 * Constructor
	 *
	 */
	public function __construct() {
		$this->register_hook_callbacks();
	}

	/**
	 * Register callbacks for actions and filters
	 *
	 */
	protected function register_hook_callbacks() {
		ActionsFilters::add_action('rest_api_init', $this, 'register_routes');
	}

	function register_routes() {
		$posts = new Posts_Route();
		$posts->register_routes();
	}
}
