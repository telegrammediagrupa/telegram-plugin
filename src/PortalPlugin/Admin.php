<?php
namespace Telegram\PortalPlugin;

class Admin extends Instance {

	public function __construct() {
	    $this->register_hook_callbacks();
	}

	public function register_hook_callbacks () {
	    ActionsFilters::add_action('admin_menu', $this, 'add_pages', 98);
		ActionsFilters::add_action('admin_menu', $this, 'menu');
	    ActionsFilters::add_filter( 'preview_post_link', $this, 'preview', 10, 2 );
	    ActionsFilters::add_filter('acf/fields/post_object/query', $this, 'acf_query', 10, 3);
	    ActionsFilters::add_filter( 'get_terms', $this, 'default_category_order', 10, 4 );
    }

    function default_category_order($terms, $taxonomy, $query_vars, $term_query) {
        if ($taxonomy[0] == 'category') {
            usort($terms, function($a, $b) {
                return $a->count < $b->count;
            });
        }
        return $terms;
    }

	function acf_query($args, $field, $post_id) {
		$args['post_status'] = ['publish'];
		$args['orderby'] = 'date';
		$args['order'] = 'desc';
		$args['no_found_rows'] = true;
		$args['ignore_sticky_posts'] = true;
		return $args;
	}

	function add_pages() {
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_sub_page(array(
				'page_title' 	=> 'Breaking',
				'menu_title'	=> 'Breaking',
				'menu_slug' 	=> 'tmg-breaking',
				'capability'	=> 'edit_posts',
				'redirect'		=> false,
                'parent_slug' => 'edit.php'
			));
		}
    }

	function preview($preview_link, $post) {
	  if ('fotogalerije' === $post->post_type) {
	    return site_url('/fotogalerije/preview/'.$post->ID);
    }
	  return site_url('/preview/'.$post->ID);
  }

	function menu() {
		//add_options_page('Dev stuff', 'Dev stuff', 'edit_posts', 'dev-stuff', array($this, 'admin_page'));

		add_posts_page('Autori', 'Autori', 'edit_others_posts', 'users.php?page=view-guest-authors');

		//add_posts_page('Large Breaks', 'Breaks', 'edit_posts', 'post.php?post=519214&action=edit');

		//add_posts_page('Promo Special', 'Special', 'edit_posts', 'post.php?post=519256&action=edit');
		//add_posts_page('Covid', 'Covid', 'edit_posts', 'post.php?post=780032&action=edit');
	}

	function specials() {
		if ( isset( $_POST['option_page'] ) && $_POST['option_page'] == 'telegram_specials' ) { //add check nonce

		}
		?>
		<div class="wrap">
			<form method="POST"><?php
				settings_fields( 'telegram_specials' );

				do_settings_sections( 'telegram_specials' );

				submit_button();
				?></form>
		</div>
		<?php
	}

	function newsletter() {
		ob_start();
		get_template_part('templates/newsletter');
		$out = ob_get_clean();
		echo $out;
		$url = 'https://inlinestyler.torchbox.com/styler/convert/';
		$fields = array(
			'returnraw' => 'true',
			'source' => $out
		);


//open connection
		$ch = curl_init();

//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

//execute post
		$result = curl_exec($ch);

//close connection
		curl_close($ch);
		echo '<code><pre>';
		echo esc_html($result);
		echo '</pre></code>';
	}

	function newsletter_html() {
		if (isset($_GET['newsletter']) && $_GET['newsletter']=='true') {
			ob_start();
			get_template_part('templates/newsletter');
			$out = ob_get_clean();
			echo $out;
			die();
		}
	}

	function options() {
		if (isset($_POST['option_page']) && $_POST['option_page'] == 'telegram_options') { //add check nonce
			$options = array(
				'breaking_status',
				'breaking_image',
				'breaking_title',
				'breaking_overtitle',
				'breaking_subtitle',
				'breaking_link',
				'breaking_parties',
				'breaking_lijevo',
				'breaking_desno',
				'breaking_lijevo_broj',
				'breaking_desno_broj'
			);

			foreach ($options as $option) {
				update_option( $option,  $_POST[$option] );
			}
			wp_cache_delete('telegram_breaking_desktop', 'widgets');
			wp_cache_delete('telegram_breaking_mobile', 'widgets');
		}
		?><div class="wrap">
		<form method="POST">
			<?php
			settings_fields( 'telegram_options' );

			do_settings_sections( 'telegram_options' );

			submit_button();
			?>
		</form>
		<script>
            jQuery(document).ready( function() {
                function media_upload() {
                    jQuery('body').on('click', '.upload_image_button', function (e) {
                        var button_id = '#' + jQuery(this).attr('id');
                        var buttonc = '#' + jQuery(this).data('id');
                        var button = jQuery(button_id);
                        wp.media.editor.send.attachment = function (props, attachment) {
                            jQuery(buttonc).val(attachment.id);
                        };
                        wp.media.editor.open(button);
                        return false;
                    });
                }

                media_upload();
            });
		</script>
		</div><?php
	}

	public function add_settings() {
		register_setting('telegram_options', 'breaking_status');
		register_setting('telegram_options', 'breaking_image');
		register_setting('telegram_options', 'breaking_title');
		register_setting('telegram_options', 'breaking_overtitle');
		register_setting('telegram_options', 'breaking_subtitle');
		register_setting('telegram_options', 'breaking_link');
		register_setting('telegram_options', 'breaking_parties' );
		register_setting('telegram_options', 'breaking_lijevo' );
		register_setting('telegram_options', 'breaking_desno' );
		register_setting('telegram_options', 'breaking_lijevo_broj' );
		register_setting('telegram_options', 'breaking_desno_broj' );
		add_settings_section('breaking', 'Breaking news', '', 'telegram_options');

		add_settings_field('breaking_status', 'Status', array( $this, 'radio' ), 'telegram_options', 'breaking', array('id' => 'breaking_status', 'options' => array('on' => 'Uključi', 'off' => 'Isključi')));
		add_settings_field('breaking_image', 'Naslovna slika', array( $this, 'input_image' ), 'telegram_options', 'breaking', array('id' => 'breaking_image'));
		add_settings_field('breaking_title', 'Naslov', array( $this, 'input' ), 'telegram_options', 'breaking', array('id' => 'breaking_title'));
		add_settings_field('breaking_overtitle', 'Nadnaslov', array( $this, 'input' ), 'telegram_options', 'breaking', array('id' => 'breaking_overtitle'));
		add_settings_field('breaking_subtitle', 'Podnaslov', array( $this, 'textarea' ), 'telegram_options', 'breaking', array('id' => 'breaking_subtitle'));
		add_settings_field('breaking_link', 'Link', array( $this, 'input' ), 'telegram_options', 'breaking', array('id' => 'breaking_link'));
		add_settings_field('breaking_parties', 'Stranke', array( $this, 'repeater' ), 'telegram_options', 'breaking', array('id' => 'breaking_parties'));
		add_settings_field('breaking_lijevo', 'Naziv lijevo', array($this, 'input'), 'telegram_options', 'breaking', array('id' => 'breaking_lijevo'));
		add_settings_field('breaking_lijevo_broj', 'Broj lijevo', array($this, 'input'), 'telegram_options', 'breaking', array('id' => 'breaking_lijevo_broj'));
		add_settings_field('breaking_desno', 'Naziv desno', array($this, 'input'), 'telegram_options', 'breaking', array('id' => 'breaking_desno'));
		add_settings_field('breaking_desno_broj', 'Broj desno', array($this, 'input'), 'telegram_options', 'breaking', array('id' => 'breaking_desno_broj'));

		//add_settings_section( 'credentials', 'API credentials', array( $this, 'credentials' ), 'telegram_options' );
	}

	function input( $args ) {
		$id = $args['id'];
		?>
		<input name="<?php echo esc_attr( $id ) ?>" size="60" id="<?php echo esc_attr( $id ) ?>" type="text" value="<?php echo esc_attr( stripslashes( get_option( $id ) ) ) ?>">
		<?php
	}

	function textarea( $args ) {
		$id = $args['id'];
		?><textarea name="<?php echo esc_attr( $id ) ?>" cols="58" rows="5" id="<?php echo esc_attr( $id ) ?>"><?php echo esc_html( stripslashes( get_option( $id ) ) ) ?></textarea><?php
	}

	function input_image( $args ) {
		$id = $args['id'];
		?>
		<input name="<?php echo esc_attr( $id ) ?>" id="<?php echo esc_attr( $id ) ?>" type="text" value="<?php echo esc_attr( get_option( $id ) ) ?>">
		<button class="upload_image_button" data-id="<?php echo esc_attr( $id ) ?>" id="upload-button">Odaberi</button>
		<label><i>&nbsp;Crop format: Velike priče</i></label>
		<?php
	}

	public function checkbox($args) {
		$value = get_option($args['id']);
		foreach ($args['options'] as $key => $option) {
			$checked = '';
			if ($key == $value) {
				$checked = 'checked';
			}
			printf( '<input type="checkbox" name="%1$s" value="%2$s" id="%2$s" %4$s><label for="%2$s">%3$s</label>', $args['id'], $key, $option, $checked );
		}
	}

	public function radio($args) {
		$value = get_option($args['id']);
		foreach ($args['options'] as $key => $option) {
			$checked = '';
			if ($key == $value) {
				$checked = 'checked';
			}
			printf( '<input type="radio" name="%1$s" value="%2$s" id="%2$s" %4$s><label for="%2$s">%3$s</label>', $args['id'], $key, $option, $checked );
		}
	}

	public function repeater($args) {
		$values = get_option($args['id']);
		$values = array_values($values);
		$key = 0;
		echo '<table id="repeat_table"><tr><th>Naziv</th><th>Boja</th><th>Koalicija</th><th>Broj mjesta</th><th>Obriši</th></tr>';
		foreach ($values as $key => $value) {
			if ($value['label']) {
				$coal = $value['coalition'];
				echo sprintf(
					'<tr id="row-%2$s-%1$d">
<td><input type="text" value="%3$s" name="%2$s[%1$d][label]"></td>
<td><input type="color" name="%2$s[%1$d][color]" value="%6$s"></td>
<td><input type="checkbox" value="1" name="%2$s[%1$d][coalition]" %5$s></td>
<td><input id="%2$s-%1$d" name="%2$s[%1$d][value]" value="%4$s"></td>
<td><a href="#" class="remove" id="%2$s-%1$d">X</a></td></tr>', $key, $args['id'], $value['label'], $value['value'], $coal?'checked':'', $value['color'] );
			}
		}
		echo '</table>';
		?>
		<button class="btn add_option">Dodaj opciju</button>
		<script>
            var id = <?php echo wp_json_encode($args['id']); ?>,
                key = <?php $key = intval($key); echo wp_json_encode( ++$key ); ?>;
            jQuery('.add_option').on('click', function(e) {
                e.preventDefault();
                var parent = jQuery('#repeat_table').append('<tr><td><input type="text" value="" name="'+id+'['+key+'][label]"></td><td><input type="color" name="'+id+'['+key+'][color]"> </td> <td><input type="checkbox" value="1" name="'+id+'['+key+'][coalition]"></td> <td><input id="'+id+'-'+key+'" name="'+id+'['+key+'][value]" value=""></td></tr>');
                key++;
            });
            jQuery('.remove').click(function(e) {
                e.preventDefault();
                var id = jQuery(this).attr('id');
                jQuery('#row-'+id).remove();
            });
		</script>
		<?php
	}

	///remove for VIP

	function admin_page() {
		?>
		<form method="post" action="">
			<input type="text" style="width: 600px;" name="cfurl" value="https://www.telegram.hr/wp-content/themes/telegram/style.css?ver=4.1.1">
			<input type="submit" value="Purge">
		</form>
		<form method="post" action="">
			<input type="text" style="width: 600px;" name="cfurl" value=https://www.telegram.hr/wp-content/themes/telegram/js/special-gallery-article.js?ver=4.1.1">
			<input type="submit" value="Purge">
		</form>
		<?php
	}

	function crop($url, $id) {
		if ($url) {
			$dot = strrpos($url,".");
			$dst_file2x = substr($url,0,$dot).'@2x'.substr($url,$dot);
			$r = wp_remote_request( 'https://api.cloudflare.com/client/v4/zones/f303d516f58c6c0a729da76b353f8fae/purge_cache', array(
				'method' => 'POST',
				'body' => json_encode(array( 'files' => [$url, $dst_file2x] )),
				'headers' => array(
					'Content-Type' => 'application/json',
					'X-Auth-Email' => 'services@telegram.hr',
					'X-Auth-Key' => '1175afbf9965f46b281ee06d9cd5e412f4095'
				),
			) );
			update_post_meta($id, 'needs_refresh', date('YmdHi'));
			//telegram_debug( $r, true );
		}
	}

	function cropped($sources, $size_array, $image_src, $image_meta, $attachment_id) {
	    $meta = get_post_meta($attachment_id, 'needs_refresh', true);
	    if ($meta) {
	        $out = [];
		    foreach ( $sources as $key => $source ) {
		        $source['url'] = $source['url'].'?v='.$meta;
                $out[$key] = $source;
		    }
		    return $out;
	    }
	    return $sources;
    }

	function purge() {
		if (isset($_POST['cfurl']) && $_POST['cfurl']) {
			$r = wp_remote_request( 'https://api.cloudflare.com/client/v4/zones/f303d516f58c6c0a729da76b353f8fae/purge_cache', array(
				'method' => 'DELETE',
				'body' => json_encode(array( 'files' => [esc_url( $_POST['cfurl'] )] )),
				'headers' => array(
					'Content-Type' => 'application/json',
					'X-Auth-Email' => 'services@telegram.hr',
					'X-Auth-Key' => '1175afbf9965f46b281ee06d9cd5e412f4095'
				),
			) );
			telegram_debug($r,true);
		}
	}
}