<?php
namespace Telegram\PortalPlugin;

class Embeds extends Instance {

    public function __construct() {
        $this->register_hook_callbacks();
    }

	public function register_hook_callbacks() {
		wp_embed_register_handler('telegram_foto', '#http:\/\/www\.telegram\.hr\/fotogalerije\/(.*)\/(.*)#i', array($this, 'embed_foto'));
		wp_embed_register_handler('telegram_interlude_short', '#http:\/\/in\.fm\/v\/(.*)#i', array($this,'interlude'));
		wp_embed_register_handler('telegram_interlude_long', '#https:\/\/interlude\.fm\/v\/(.*)#i', array($this,'interlude'));
		wp_embed_register_handler('telegram_vidme', '#https:\/\/vid\.me\/(.*)#i', array($this,'vidme'));
		wp_embed_register_handler('telegram_revolt', '#http://revolt.tv/videos/(.*)#i', array($this,'revolt'));
		wp_embed_register_handler('telegram_vkcom', '#http://vk.com/video(.*)_(.*)?hash=(.*)#i', array($this,'vkcom'));
		wp_embed_register_handler('telegram_imdb', '#http://www.imdb.com/video/imdb/(.*)#i', array($this,'imdb'));
		wp_embed_register_handler('telegram_ooyala', '#http://player.ooyala.com/iframe.html(.*)#i', array($this,'ooyala'));
		wp_embed_register_handler('telegram_indiewire', '#http://www.indiewire.com/embed/player.jsp\?videoId=(.+?)&width=([\d]+)#i', array($this,'indiewire'));

		wp_embed_register_handler('telegram_jwplayer', '#http://dashboard.jwplatform.com/videos/(.*)#i', array($this,'jwplayer'));
		wp_embed_register_handler('telegram_jwplayer_playlist', '#http://dashboard.jwplatform.com/channels/(.*)#i', array($this,'jwplayer'));
		wp_embed_register_handler( 'adriaticmedia_video', '#http:\/\/video\.adriaticmedia\.hr\/videos\/(.*?)[\/]?$#i', array( $this, 'video' ) );
		wp_embed_register_handler( 'rtl_video', '#https:\/\/www.rtl.hr\/(.*?)[\/]?$#i', array( $this, 'rtl_video' ) );
	}

	function rtl_video($matches, $attr, $url, $rawattr) {
        return '<iframe height="410" src="'.str_replace('/video/', '/embed/video/', $url).'?src=telegram" width="710"></iframe>';
    }
	function vidme($matches, $attr, $url, $rawattr) {
		return '<iframe src="https://vid.me/e/'.$matches[1].'" width="656" height="365" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen scrolling="no"></iframe>';
	}
	function revolt($matches, $attr, $url, $rawattr) {
		return "<iframe src='https://revolt.tv/embed/".$matches[1]."' class='special-left' seamless='seamless' scrolling='no' height='370' width='656' allowfullscreen></iframe>";
	}

	function vkcom($matches, $attr, $url, $rawattr) {
      global $content_width;
		$width = $content_width;
		if ($width == 560) {
			$class = '';
			$width = 560;
			$height = 315;
		}
		else {
			$class = 'class="special-left"';
			$width = 656;
			$height = 369;
		}
		return '<iframe '.$class.' src="https://vk.com/video_ext.php?oid='.$matches[1].'&id='.$matches[2].'&hash='.$matches[3].'&hd=3" width="'.$width.'" height="'.$height.'" frameborder="0"></iframe>';

	}
	function videahu($matches, $attr, $url, $rawattr) {
	  global $content_width;
	  $width = $content_width;
		if ($width == 560) {
			$class = '';
			$width = 560;
			$height = 315;
		}
		else {
			$class = 'class="special-left"';
			$width = 656;
			$height = 369;
		}
		$id = explode('-', $matches[2]);
		$id = array_pop($id);
		return '<iframe '.$class.' width="'.$width.'" height="'.$height.'" src="http://videa.hu/player?v='.$id.'" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" frameborder="0"></iframe>';

	}
	function imdb($matches, $attr, $url, $rawattr) {
	  global $content_width;
	  $width = $content_width;
		if ($width == 560) {
			$class = '';
			$width = 560;
			$height = 315;
		}
		else {
			$class = 'class="special-left"';
			$width = 656;
			$height = 369;
		}

		return '<iframe '.$class.' src="http://www.imdb.com/video/imdb/'.$matches[1].'/imdb/embed?autoplay=false&width='.$width.'" width="'.$width.'" height="'.$height.'" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true" frameborder="no" scrolling="no"></iframe>';

	}
	function ooyala($matches, $attr, $url, $rawattr) {
	  global $content_width;
	  $width = $content_width;
		if ($width == 560) {
			$class = '';
			$width = 560;
			$height = 315;
		}
		else {
			$class = 'class="special-left"';
			$width = 656;
			$height = 369;
		}

		return '<script height="'.$height.'" width="'.$width.'" src="'.str_replace('.html', '.js', $matches[0]).'"></script>';

	}
	function rutube($matches, $attr, $url, $rawattr) {
	  global $content_width;
	  $width = $content_width;
		if ($width == 560) {
			$class = '';
			$width = 560;
			$height = 315;
		}
		else {
			$class = 'class="special-left"';
			$width = 656;
			$height = 369;
		}

		return '<iframe '.$class.' width="'.$width.'" height="'.$height.'" src="//rutube.ru/play/embed/'.$matches[1].'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>';

	}
	function indiewire($matches, $attr, $url, $rawattr) {
	  global $content_width;
	  $width = $content_width;  
		if ($width == 560) {
			$class = '';
			$width = 560;
			$height = 315;
		}
		else {
			$class = 'class="special-left"';
			$width = 656;
			$height = 369;
		}
		$id = $matches[1];
		return '<iframe width="'.$width.'" height="'.$height.'" src="https://www.indiewire.com/embed/player.jsp?videoId='.$id.'&width='.$width.' frameborder="0" scrolling="0" allowfullscreen/>';

	}
	function jwplayer( $matches, $attr, $url, $rawattr ) {
		$video = $matches[1];
		ob_start();
		?><video poster="https://video.adriaticmedia.hr/thumbnails/<?php echo esc_attr( $video ) ?>.jpg" id="v-<?php echo esc_attr( $video ) ?>" class="video-js vjs-default-skin" controls preload="auto" style="width: 100%">
			<source src="https://video.adriaticmedia.hr/originals/jw/videos/<?php echo rawurlencode( $video ) ?>.mp4" type="video/mp4" />
		</video><?php

		return ob_get_clean();
	}
	function embed_foto($matches, $attr, $url, $rawattr) {

		return "[galerija url=$url]";
	}
	function interlude($matches, $attr, $url, $rawattr) {

		return '<iframe width="656" height="369" src="https://in.fm/embed/'.$matches[1].'" frameborder="0" allowfullscreen></iframe>';
	}

	function video( $matches, $attr, $url, $rawattr ) {
		$video = $matches[1];

		ob_start();
		?>
		<video poster="https://video.adriaticmedia.hr/thumbnails/<?php echo esc_attr( $video ) ?>.jpg" id="v-<?php echo esc_attr( $video ) ?>" class="video-js vjs-default-skin" controls preload="auto">
			<source src="https://video.adriaticmedia.hr/videos/<?php echo rawurlencode( $video ) ?>.mp4" type="video/mp4" />
		</video>
		<?php

		return ob_get_clean();
	}

}