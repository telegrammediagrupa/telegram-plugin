<?php

namespace Telegram\PortalPlugin;

class Cxense extends Instance {

	private $username = 'marko@telegramgrupa.hr';
	private $api_key = 'api&user&ozZ/Z6OyG0KU9fx9p34qoQ==';
	private $site_id = '1128464677385494954';

	public function __construct() {
	}

	protected function register_hook_callbacks() {
		// TODO: Implement register_hook_callbacks() method.
	}

	private function getApi($path, $obj) {
		$date = date('Y-m-d\TH:i:s.000O');
		$signature = hash_hmac("sha256", $date, $this->api_key);
		$url = 'https://api.cxense.com'.$path;
		$date = date('Y-m-d\TH:i:s.000O');
		return wp_remote_post($url, [
			'headers' => [
				'Content-Type' => 'application/json',
				'X-cXense-Authentication' => 'username=' . $this->username . ' date='. $date.' hmac-sha256-hex='.$signature
			],
			'body' => json_encode($obj)
		]);
	}

	public function getTraffic() {
		$tz = new \DateTimeZone('Europe/Zagreb');
		$date = new \DateTime('yesterday 16:00:00', $tz);
		$start = $date->getTimestamp();
		$stop = new \DateTime('now', $tz);
		$stop  = $stop->getTimestamp();
		$obj = [
			'siteId' => $this->site_id,
			'start' => $start,
			'stop' => $stop,
			'fields' => [
				'events'
			],
			'groups' => ['url'],
			'count' => 40
		];
		$r = $this->getApi('/traffic/event', $obj);
		$data = json_decode($r['body'],true);
		$out = [];
		foreach ($data['groups'][0]['items'] as $one) {
			$link = $one['item'];
			$slug = explode('/', $link);
			if (count($slug) === 6) {
				$slug = $slug[4];
				$id = $this->get_post_by_slug($slug);
				if ($id) {
					$out[] = $id;
				}
			}
		}
		if ($out) {
			update_option('telegram_cxense_most_read', $out);
		}
	}

	private function get_post_by_slug($slug) {
		global $wpdb;
		$id = false;
		if ($slug) {
			$id = $wpdb->get_var( "SELECT ID FROM {$wpdb->posts} WHERE  post_name LIKE '{$slug}' AND post_date > DATE_SUB(CURDATE(), INTERVAL 1 DAY) LIMIT 1" );
		}
		return $id;
	}
}
