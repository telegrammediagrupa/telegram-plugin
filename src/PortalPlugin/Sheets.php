<?php
namespace Telegram\PortalPlugin;


class Sheets {
  protected $client;

  public function __construct()
  {
    $this->client = $this->loadClient();
  }

  public function loadClient() {
    $key_file_location = '/home/marko/telegram/Telegram-618e00d5592f.json';

    $client = new \Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(\Google_Service_Sheets::SPREADSHEETS_READONLY);
    $client->setAuthConfig($key_file_location);
    $client->setAccessType('offline');
    return $client;
  }

  public function getSheetData() {
    $service = new \Google_Service_Sheets($this->client);
    $spreadsheetId = '1j69ejJhk14AEHS4KaAZFtrfIgeUNm72UiABdl2uF638';
    $range = 'Sheet1!A:E';
    $result = $service->spreadsheets_values->get($spreadsheetId, $range);
    $numRows = $result->getValues() != null ? count($result->getValues()) : 0;
    wp_cache_set('sheet_1j69ejJhk14AEHS4KaAZFtrfIgeUNm72UiABdl2uF638_count', $numRows-1, 'sheets');
    $messages = [];
    foreach ($result->getValues() as $value) {
      if (isset($value[4]) && $value[4] == 's') {
        $messages[] = $value;
      }
    }
    wp_cache_set('sheet_1j69ejJhk14AEHS4KaAZFtrfIgeUNm72UiABdl2uF638_messages', $messages, 'sheets');
  }
}