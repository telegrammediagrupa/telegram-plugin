<?php

namespace Telegram\PortalPlugin;

class Authors {

	public function generate_stats() {
		global $wpdb;
		$q = new \WP_Query(array(
			'posts_per_page' => -1,
			'post_type' => array('guest-author'),
			'post_status' => 'publish'
		));
		while ($q->have_posts()) {
			$q->the_post();
			$linked = get_post_meta($q->post->ID, 'cap-linked_account', true);
			if ($linked) {
				\WP_CLI::line('autor: ' . $linked);
				$user = $wpdb->get_var('SELECT ID FROM wp_users WHERE user_login = "'.$linked.'"');
				$count = count_user_posts($user, array('post', 'price', 'fotogalerija', 'video'), true);
				\WP_CLI::line('user_posts: ' . $count);
				update_post_meta($q->post->ID, 'user_posts', intval($count));
			}
		}
	}
}