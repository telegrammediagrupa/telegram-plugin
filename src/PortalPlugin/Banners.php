<?php

namespace Telegram\PortalPlugin;

class Banners extends Instance {

	public function __construct() {
		$this->register_hook_callbacks();
	}

	public function register_hook_callbacks() {
		ActionsFilters::add_action( 'wp_footer', $this, 'telegram_xclaim_footer' );
	}

	function telegram_xclaim_footer() {
		if ( get_query_var( 'audit' ) ) {
			return false;
		}
		if ( wp_is_mobile() && is_single() && ( ! ( $oglasi = get_field( 'oglasi' ) ) || ! in_array( 'xclaim', $oglasi ) ) ) {
			?>
            <script src="//hr-engine.xclaimwords.net/script.aspx?partnerid=200185" language="javascript"
                    type="text/javascript"></script>
            <script>
                function onXClaimSettingsLoaded() {
                }
            </script>
			<?php
		}
	}
}