<?php

namespace Telegram\PortalPlugin;

class Audit extends Instance {

	private $QUERY_VAR = 'audit';

	/**
	 * Constructor
	 *
	 */
	public function __construct() {
		$this->register_hook_callbacks();
	}

	/**
	 * Register callbacks for actions and filters
	 *
	 */
	protected function register_hook_callbacks() {
		ActionsFilters::add_action('init', $this, 'init');
		//ActionsFilters::add_filter( 'query_vars', $this,'custom_query_vars_filter' );
		ActionsFilters::add_filter( 'request', $this, 'force_query_var_value' );
	}

	public function init() {
		add_rewrite_endpoint( $this->QUERY_VAR, EP_ALL );
	}
	public function add_query_var($vars) {
		$vars[] = 'audit';
		return $vars;
	}

	public function force_query_var_value( $query_vars ) {
		if ( isset( $query_vars[ $this->QUERY_VAR ] ) && '' === $query_vars[ $this->QUERY_VAR ] ) {
			$query_vars[ $this->QUERY_VAR ] = 1;
		}
		return $query_vars;
	}
}